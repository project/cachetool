<?php

namespace Drupal\cachetool\Controller;

use CacheTool\Command\ApcuCacheInfoCommand;
use CacheTool\Command\OpcacheStatusCommand;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\DependencyInjection\ContainerInterface;
use CacheTool\Adapter\FastCGI;
use CacheTool\CacheTool;
use CacheTool\Proxy;

/**
 * Cachetool Report page.
 *
 * Display status and statistics about APCu and OPcache.
 */
class ReportController extends ControllerBase {

  protected ContainerInterface$container;

  /**
   * ReportController constructor.
   */
  public function __construct(ContainerInterface $container) {
    $adapter = new FastCGI();
    $cachetool = new CacheTool();
    $cachetool->setAdapter($adapter);
    $cachetool->addProxy(new Proxy\PhpProxy());
    $cachetool->addProxy(new Proxy\ApcuProxy());
    $cachetool->addProxy(new Proxy\OpcacheProxy());
    $container->set('cachetool', $cachetool);
    $this->container = $container;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ReportController|static {
    return new static($container);
  }

  /**
   * Cachetool report overview.
   *
   * @throws \Symfony\Component\Console\Exception\ExceptionInterface
   */
  public function overview(): array {

    include_once DRUPAL_ROOT . '/core/includes/install.inc';

    $input = new StringInput('');
    $output = new BufferedOutput();

    $command = new ApcuCacheInfoCommand();
    $command->setContainer($this->container);
    $command->run($input, $output);

    $build['report'] = [
      '#type' => 'status_report',
      '#requirements' => [
        'apcu' => [
          'title' => $this->t('APCu Cache Info'),
          'value' => ['#markup' => '<pre>' . $output->fetch() . '</pre>'],
          'severity_status' => 'warning',
        ],
      ],
    ];

    $command = new OpcacheStatusCommand();
    $command->setContainer($this->container);
    $command->run($input, $output);

    $build['report']['#requirements']['opcache'] = [
      'title' => $this->t('OPcache Status'),
      'value' => ['#markup' => '<pre>' . $output->fetch() . '</pre>'],
      'severity_status' => 'warning',
    ];

    return $build;
  }

}
